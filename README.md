Javascript implementation of self-avoiding random walks ("polymers") on 2d lattices.

So far: square and hexagonal (regular) lattices.

And a particular model of "polymer growth", animated.

See it live at [https://rjb-polymers.surge.sh/](https://rjb-polymers.surge.sh/).