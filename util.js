// UTILITY DEFINITIONS
'use strict';

const TWO_PI = 2 * Math.PI;
const randint = N => (Math.random() * N) | 0;

const shuffled = arr => {
    const out = [];
    for (let i = arr.length; i > 0; --i) {
        const [v] = arr.splice(randint(i), 1);
        out.push(v);
    }
    return out;
};

function* randomizedPoints2d(W, H) {
    const numbers = [...new Array(W * H).keys()];
    for (let i = W * H; i > 0; --i) {
        const [num] = numbers.splice(randint(i), 1);
        yield [num % W, (num / W) | 0];
    }
}

const Colors = (
    COLORS = [
        '#1f77b4',
        '#ff7f0e',
        '#2ca02c',
        '#d62728',
        '#9467bd',
        '#8c564b',
        '#e377c2',
        '#7f7f7f',
        '#bcbd22',
        '#17becf'
    ] // matplolib default colors
) => {
    const L = COLORS.length;
    let colorIdx = -1;
    return () => COLORS[++colorIdx % L];
};

export { TWO_PI, randint, shuffled, randomizedPoints2d, Colors };
