# Description of the algorithm

The arena is a lattice of `WxH` points arranged on a lattice, square by default,
but hexagonal is provided as well. The neighbors of a point are those separated
from it by a single lattice link.

1. Choose a point at random, without replacement. Mark it as *visited*.
2. Choose one of its neighbors, at random, among those not yet *visited*. Mark
   it as *visited*. If there are none fulfilling this condition, the current
   point is a *disconnected atom* and is marked by a dot. Otherwise, the atom
   becomes *connected* to the neighbor, the neighbor is added to the set of
   *visited* sites, and the same procedure continues with the neighbor. Paint
   the links between *connected* sites.
3. Once it is no longer possible to extend the chain of *connected* sites, go to
   step 1. If there are no more not *visited* points left, we are done.
