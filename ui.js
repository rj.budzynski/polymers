import { Colors, TWO_PI } from './util.js';
import { PolymersHex2d } from './polymers.js';

export class MyCanvas {
    constructor(canvas, { W = 40, H = 30, scale = 20 } = {}) {
        canvas.width = scale*W;
        canvas.height = scale*H
        const gc = canvas.getContext('2d');
        canvas.style.visibility = 'visible';
        Object.assign(this, {canvas, scale, W, H, gc})
    }

    init() {
        const { gc, scale, canvas } = this;
        gc.resetTransform();
        gc.clearRect(0, 0, canvas.width, canvas.height);
        gc.setTransform(scale, 0, 0, scale, scale / 2, scale / 2);
        gc.fillStyle = 'white';
        gc.lineJoin = 'round';
        gc.lineWidth = 0.3;
        return this;
    }

    drawCircle(p) {
        const gc = this.gc;
        gc.beginPath();
        gc.arc(...p, 0.25, 0, TWO_PI);
        gc.fill();
        gc.closePath();
        return this;
    }

    drawLink(p, q) {
        const gc = this.gc;
        gc.beginPath();
        gc.moveTo(...p);
        gc.lineTo(...q);
        gc.closePath();
        gc.stroke();
        return this;
    }

    run(polymers, frameDelay = 20) {
        const { gc, scale, W, H } = this;
        if (polymers instanceof PolymersHex2d) {
            const sc = (scale * W) / (W - 0.5);
            gc.setTransform(sc, 0, 0, sc, sc / 2, sc / 2);
        }
        const nextColor = Colors();
        this.gc.strokeStyle = nextColor();
        polymers.init();
        const events = polymers.run();
        const nextFrame = ev => {
            if (ev.done) {
                this.isRunning = false;
                alert('all done!');
                return;
            }
            const val = ev.value;
            if (val.length === 1) {
                this.drawCircle(val[0]);
            } else if (val.length === 2) {
                this.drawLink(...val);
            } else if (val === 'chain') {
                gc.strokeStyle = nextColor();
            } else {
                throw new Error(`unhandled event: ${ev}`);
            }
            setTimeout(nextFrame, frameDelay, events.next());
        };
        setTimeout(nextFrame, frameDelay, events.next());
        return true;
    }
}
