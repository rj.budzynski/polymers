import { shuffled, randomizedPoints2d } from './util.js';

export class Polymers2d {
    constructor(W, H) {
        this.atoms = new Set();
        this.points = function*() {};
        Object.assign(this, { W, H });
    }

    init() {
        this.atoms.clear();
        this.points = randomizedPoints2d(this.W, this.H);
    }

    neighborsOf(p) {
        const [i, j] = p,
            { W, H } = this;
        const out = [];
        if (i > 0) out.push([i - 1, j]);
        if (i < W - 1) out.push([i + 1, j]);
        if (j > 0) out.push([i, j - 1]);
        if (j < H - 1) out.push([i, j + 1]);
        return out;
    }

    notVisited(p) {
        const [i, j] = p;
        for (const [k, l] of this.atoms) {
            if (i === k && j === l) return false;
        }
        return true;
    }

    linkFrom(p) {
        const neighbors = shuffled(this.neighborsOf(p));
        for (const q of neighbors) {
            if (this.notVisited(q)) {
                this.atoms.add(q);
                return q;
            }
        }
        return null;
    }

    *chainFrom(p) {
        const chain = [p];
        this.atoms.add(p);
        while (p !== null) {
            p = this.linkFrom(p);
            if (p) {
                chain.push(p);
                yield chain.slice(chain.length - 2);
            }
        }
        if (chain.length === 1) {
            yield chain;
        } else {
            yield 'chain';
        }
    }

    *run() {
        for (const p of this.points) {
            if (this.notVisited(p)) yield* this.chainFrom(p);
        }
    }
}

export class PolymersHex2d extends Polymers2d {
    neighborsOf(p) {
        const out = super.neighborsOf(p);
        const [i, j] = p;
        const im = i - 1,
            ip = i + 1,
            jm = j - 1,
            jp = j + 1,
            { W, H } = this;
        if (j % 2 === 0) {
            if (im > 0 && jp < H) out.push([im, jp]);
            if (im > 0 && jm > 0) out.push([im, jm]);
        } else {
            if (ip < W && jp < H) out.push([ip, jp]);
            if (ip < W && jm > 0) out.push([ip, jm]);
        }
        return out;
    }

    *run() {
        const SIN_PHI = Math.sqrt(3) / 2;
        const hexCoords = p => {
            const [i, j] = p;
            const x = (j % 2) / 2 + i;
            const y = j * SIN_PHI;
            return [x, y];
        };

        for (const ch of super.run()) {
            if (Array.isArray(ch)) yield ch.map(hexCoords);
            else yield ch;
        }
    }
}
