import { Polymers2d, PolymersHex2d } from './polymers.js';
import { MyCanvas } from './ui.js';

const main = () => {
    const picture = new MyCanvas(document.querySelector('canvas'), {
        W: 40,
        H: 30
    });

    const { W, H } = picture;
    let polymers;

    const start = url => {
        const type = (url.hash && url.hash.replace('#', '')) || 'square';
        let w = W,
            h = H;
        if (type === 'hex') {
            --w;
            h = ((H * 2) / Math.sqrt(3)) | 0;
        }
        polymers = new (type === 'hex' ? PolymersHex2d : Polymers2d)(w, h);
        picture.isRunning = picture.init().run(polymers);
    };

    document.querySelectorAll('#restart a').forEach(el =>
        el.addEventListener('click', e => {
            e.preventDefault();
            if (picture.isRunning) return;
            start(new URL(e.target.href));
            document.location = e.target.href;
        })
    );

    start(new URL(document.URL));
};

window.addEventListener('load', main);
